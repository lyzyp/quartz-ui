﻿/*******************************************************************************
 * Copyright © 2020 QuartzUI.Framework 版权所有
 * Author: QuartzUI
 * Description: QuartzUI快速开发平台
 * Website：
*********************************************************************************/
using QuartzUI.Code;
using QuartzUI.Domain.SystemOrganize;
using System;
using System.Threading.Tasks;
using QuartzUI.DataBase;

namespace QuartzUI.Service.SystemOrganize
{
	public class UserService : DataFilterService<UserEntity>, IDenpendency
    {
        /// <summary>
        /// 缓存操作类
        /// </summary>
        private string cacheKeyOperator = GlobalContext.SystemConfig.ProjectPrefix + "_operator_";// +登录者token
                                                                                                  //获取类名

        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public async Task<UserEntity> GetFormExtend(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        /// <summary>
        /// 登录判断
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserEntity> CheckLogin(string username, string password)
        {
            UserEntity userEntity = await unitofwork.GetDbClient().Queryable<UserEntity>().SingleAsync(a => a.Account == username);
            if (userEntity != null)
            {
                if (userEntity.EnabledMark == true)
                {
                    //缓存用户账户信息
                    var userLogOnEntity = await CacheHelper.GetAsync<OperatorUserInfo>(cacheKeyOperator + "info_" + userEntity.Id);
                    if (userLogOnEntity == null)
                    {
                        userLogOnEntity = new OperatorUserInfo();
                        UserLogOnEntity entity = await unitofwork.GetDbClient().Queryable<UserLogOnEntity>().InSingleAsync(userEntity.Id);
                        userLogOnEntity.UserPassword = entity.UserPassword;
                        userLogOnEntity.UserSecretkey = entity.UserSecretkey;
                        userLogOnEntity.AllowEndTime = entity.AllowEndTime;
                        userLogOnEntity.AllowStartTime = entity.AllowStartTime;
                        userLogOnEntity.AnswerQuestion = entity.AnswerQuestion;
                        userLogOnEntity.ChangePasswordDate = entity.ChangePasswordDate;
                        userLogOnEntity.FirstVisitTime = entity.FirstVisitTime;
                        userLogOnEntity.LastVisitTime = entity.LastVisitTime;
                        userLogOnEntity.LockEndDate = entity.LockEndDate;
                        userLogOnEntity.LockStartDate = entity.LockStartDate;
                        userLogOnEntity.LogOnCount = entity.LogOnCount;
                        userLogOnEntity.PreviousVisitTime = entity.PreviousVisitTime;
                        userLogOnEntity.Question = entity.Question;
                        userLogOnEntity.Theme = entity.Theme;
                        await CacheHelper.SetAsync(cacheKeyOperator + "info_" + userEntity.Id, userLogOnEntity);
                    }
                    if (userLogOnEntity == null)
                    {
                        throw new Exception("账户未初始化设置密码,请联系管理员");
                    }
                    string dbPassword = Md5.md5(DESEncrypt.Encrypt(password.ToLower(), userLogOnEntity.UserSecretkey).ToLower(), 32).ToLower();
                    if (dbPassword == userLogOnEntity.UserPassword)
                    {
                        DateTime lastVisitTime = DateTime.Now;
                        int LogOnCount = (userLogOnEntity.LogOnCount).ToInt() + 1;
                        if (userLogOnEntity.LastVisitTime != null)
                        {
                            userLogOnEntity.PreviousVisitTime = userLogOnEntity.LastVisitTime.ToDate();
                        }
                        userLogOnEntity.LastVisitTime = lastVisitTime;
                        userLogOnEntity.LogOnCount = LogOnCount;
                        userLogOnEntity.UserOnLine = true;
                        await CacheHelper.RemoveAsync(cacheKeyOperator + "info_" + userEntity.Id);
                        await CacheHelper.SetAsync(cacheKeyOperator + "info_" + userEntity.Id, userLogOnEntity);
                        await OperatorProvider.Provider.ClearCurrentErrorNum();
                        return userEntity;
                    }
                    else
                    {
                        throw new Exception("密码不正确，请重新输入");
                    }
                }
                else
                {
                    throw new Exception("账户被系统锁定,请联系管理员");
                }
            }
            else
            {
                throw new Exception("账户不存在，请重新输入");
            }
        }
    }
}
