﻿using QuartzUI.Web;
using Microsoft.AspNetCore.Mvc;

namespace WaterCloud.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        [HandlerLogin]
        public ActionResult Index()
        {
            ViewBag.SqlMode = "";
            ViewBag.ProjectName = "Quartz UI";
            ViewBag.LogoIcon = "../icon/favicon.ico";
            return View();
        }
        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }
    }
}
